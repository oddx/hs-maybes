module Main where

-- prints "Yeah, ok!!" through monad composition via bind (>>=)
-- shoving Nothing into this composition rather than "Yeah" results in Nothing
main :: IO ()
main = print $ Just "Yeah" >>= maybeAppendOk >>= maybeAppendExclamation

maybeAppendOk :: String -> Maybe String
maybeAppendOk x = Just (x ++ ", ok")

maybeAppendExclamation :: String -> Maybe String
maybeAppendExclamation x = Just (x ++ "!!")
