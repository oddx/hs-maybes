module Main where

--prints "Just 7"
main :: IO ()
main = print $ calcIfVal (+) (Just 3) (Just 4)

-- technically, m could be an applicative f.
-- the point of this program is to demonstrate that
-- monads *are* applicatives with additional structure
calcIfVal :: Monad m => (a1 -> a2 -> b) -> m a1 -> m a2 -> m b
calcIfVal f x y = f <$> x <*> y
